function show(elem) {
    elem.classList.remove('fa-eye-slash');
    elem.classList.add('fa-eye');
    elem.previousElementSibling.setAttribute('type', 'text');
}

function hide(elem) {
    elem.classList.remove('fa-eye');
    elem.classList.add('fa-eye-slash');
    elem.previousElementSibling.setAttribute('type', 'password');
}

const form = document.querySelector('.password-form');
const pwd1 = document.querySelector('.password-1');
const pwd2 = document.querySelector('.password-2');
const err = document.querySelector('.error');

let showPwd = 0;

form.onclick = (e) => {
  let target = e.target;

  if (target.classList.contains('icon-password')) {
      if (showPwd === 0) {
          show(target);
          showPwd = 1;
      } else {
          hide(target);
          showPwd = 0;
      }
  }
  if (target.classList.contains('btn')) {
      if (pwd1.value === pwd2.value) {
          alert('You are welcome!');
      } else {
        err.style.display = 'block';
        target.onclick = () => {
            err.style.display = 'none';
        }
      }
  }
  return false;
};

